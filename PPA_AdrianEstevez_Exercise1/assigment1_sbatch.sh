#!/bin/bash
# on CESGA’s FT-II system.
#----------------------------------------------------
#SBATCH -J ppa_assigment1
#SBATCH -o %x-%j.log
#SBATCH -e %x-%j.err
#SBATCH -n 1
#SBATCH -c 24
#SBATCH -p cola-corta
#SBATCH -t 01:30:00

# Total execution time ~45m
module load tbb
make

echo ---------  assigment with one thread  ---------
for _ in 0 1 2; do # 3 runs per test
	OMP_NUM_THREADS=1 ./assigment1.o
done
echo ==================================================

for nthreads in 2 4 8 16 24; do
	echo ---------  assigment with ${nthreads} threads  ---------
	for limit in 1 2 4 6 8; do
		for _ in 0 1 2; do # 3 runs per test
			OMP_NUM_THREADS=$nthreads ./assigment1.o ${limit}
			echo
		done
	done
done

echo "DONE!"
echo "#-----------------------"
