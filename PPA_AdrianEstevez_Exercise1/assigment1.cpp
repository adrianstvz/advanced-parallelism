/*
 * Multithreaded Search on Unbalanced Tree
 * 
 * By: 		Adrian Estevez Barreiro
 * Mail:	adrian.estevez1@udc.es
 * Year: 	2021
 * */

#include <cstdio>
#include <cstdlib>
#include <climits>
#include <random>
#include <cmath>
#include <chrono>
#include <functional>
#include <vector>
#include <thread>
#include <tbb/tbb.h>


static int NChildren = 2;
static int Nnodes = 0;
static int LIMIT = 4;

int NThreads;

using mclock_t = std::chrono::high_resolution_clock;

struct Node_t {
	size_t value;
	std::vector< Node_t *> children;

	Node_t( int nchildren );
};

Node_t::Node_t( int nchildren ) : 
children( nchildren, nullptr ) {
	static std::mt19937_64 gen( 981 );
	static std::uniform_int_distribution< size_t > dist( 0, ULLONG_MAX );

	value = dist( gen );
	Nnodes++;
}

Node_t *
build_node( int cur_level, int nlevels ) {

	const int nchildren_limit = NChildren + ceilf( ( nlevels - cur_level ) / 2.5f );
	const int my_nchildren = ( cur_level < nlevels ) ? NChildren : 0;
	Node_t *const root = new Node_t( my_nchildren );

	if ( my_nchildren && ( root->value < ( ULLONG_MAX / 49 ) ))
		NChildren++;

	for ( int i = 0; i < my_nchildren; i++ )
		root->children[i] = build_node( cur_level + 1, nlevels );

	if ( NChildren > nchildren_limit ) 
		NChildren = nchildren_limit;

	return root;

}


Node_t *
generateTree( int nlevels ) {
	
	return build_node( 0, nlevels );

}

// Sequential implementation of findValidMax (original)
size_t
findValidMax_seq( std::vector< int >& path, Node_t *root, 
			const std::vector< std::function<bool( size_t )> >&  conditions ) {

	std::vector< int > local_path;
	std::vector< int > best_path;

	size_t local_max = root->value;

	for ( const std::function< bool(int) >& condition : conditions ) {
		if ( !condition( root->value ) ) {
			local_max = 0;
			break;
		}
	}

	for ( int i = 0; i < root->children.size(); i++ ) {

		local_path.push_back( i );

		size_t tmp = findValidMax_seq( local_path, root->children[i], conditions );

		if ( tmp > local_max ) {
			local_max = tmp;
			best_path = local_path;
		}

		local_path.clear();

	}

	path.insert( path.end(), best_path.begin(), best_path.end() );

	return local_max;

}

size_t
findValidMax( std::vector< int >& path, Node_t *root, int depth, 
			const std::vector< std::function<bool( size_t )> >&  conditions ) {

	std::vector< int > best_path;

	size_t local_max = root->value;

	for ( const std::function< bool(int) >& condition : conditions ) {
		if ( !condition( root->value ) ) {
			local_max = 0;
			break;
		}
	}

	int nchildren = (int) root->children.size();

	// Only execute on parallel if we are over under limit depth
	if ( ++depth < LIMIT ) {

		tbb::parallel_for( 
				0,
				nchildren,
				[&]( int i ) {
					std::vector< int > local_path;
					local_path.push_back( i );

					// Call findValidMax for each child
					size_t tmp = findValidMax( local_path, root->children[i], depth, conditions );

					// Find max value and best path
					if ( tmp > local_max ) {
						local_max = tmp;
						best_path = local_path;
					}
				});

	} else { // If we are beyond limit execute sequential search
		
		return findValidMax_seq( path, root, conditions );

	}

	path.insert( path.end(), best_path.begin(), best_path.end() );
	return local_max;

}

size_t 
getPathVal( const std::vector< int >& path, Node_t* root ) {

	if ( path.empty() ) {
		return root->value;
	} else {
		std::vector< int > tmp( path.begin() + 1, path.end() );
		return getPathVal( tmp, root->children[ path[0] ]);
	}

}

void tbb_set_threads(int nthreads) {
	static tbb::task_scheduler_init *init = nullptr;
	if(init) delete init;
	init = new tbb::task_scheduler_init(nthreads);
}

int
main ( int argc, char** argv ) {

	mclock_t::time_point t0, t1;

	NThreads = std::thread::hardware_concurrency();
	if ( getenv( "OMP_NUM_THREADS" ) )
		NThreads = atoi( getenv( "OMP_NUM_THREADS" ) );

	int tmp;
	if ( argc > 1 )
		if ( tmp = atoi(argv[1]) ) 
			LIMIT = tmp;

	printf( "LIMIT is %d\n", LIMIT );

	tbb_set_threads( NThreads );

	t0 = mclock_t::now();
	Node_t *const root = generateTree( 7 );
	t1 = mclock_t::now();

	printf( "Nnodes = %d Creation time = %lf\n",
			Nnodes, std::chrono::duration< double >( t1 - t0 ).count() );

	std::vector< int > path;
	std::vector< std::function< bool( size_t ) >> conditions = {
		[] ( size_t v ) { return !( v % 3 ); },
		[] ( size_t v ) { return v < 1200000000000; },
		[] ( size_t v ) { return ( v & 0xfff00 ) < 0xff000; }
	};

	t0 = mclock_t::now();
	const size_t max = findValidMax( path, root, 0, conditions );
	t1 = mclock_t::now();

	printf( "Search Time = %lf\n", std::chrono::duration< double >( t1 - t0 ).count() );
	printf( "Value       = %zu\n", max );

	const size_t gold = getPathVal( path, root );
	printf( "Test %s\n", ( max == gold ) ? "OK" : "FAILED" );

	return 0;

}

