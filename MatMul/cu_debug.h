/*
 * Header file used on the HeterogeneusProgramming assigments
 *
 * Debugging, type declarations, and common functions
 *
 */

// {{{ types --

typedef unsigned int uint ;

#ifdef _DOUBLE_
typedef double basetype;
#define tname "double"
#elif _INT_
#define tname "int"
#else
typedef float basetype;
#define tname "float"
#endif

// }}}

// {{{ functions

void check_params ( uint *, uint * ) ;
void device_info ( void ) ;

#define max(a,b) ( (a>b) ? (a) : (b) )
#define min(a,b) ( (a>b) ? (b) : (a) )

// }}}

// {{{ colors --
#define _red "\033[0;31m"
#define _green "\033[0;32m"
#define _yellow "\033[0;33m"
#define _blue "\033[0;34m"
#define _color _red
#define _reset "\033[0m"
// }}}

// {{{ time --
#include <sys/time.h>
#include <sys/resource.h>

#ifdef _noWALL_
typedef struct rusage resnfo;
typedef struct _timenfo {
  double time;
  double systime;
} timenfo;
#define timestamp(sample) getrusage(RUSAGE_SELF, (sample))
#define printtime(t) printf("%15f s (%f user + %f sys) ",		\
			    t.time + t.systime, t.time, t.systime);
#else
typedef struct timeval resnfo;
typedef double timenfo;
#define timestamp(sample)     gettimeofday((sample), 0)
#define printtime(t) printf("%s%15f s %s", t, _blue, _reset);
#endif

void myElapsedtime ( resnfo, resnfo, timenfo * ) ;

// }}}

// {{{ _DEBUG_ --

#ifdef _DEBUG_
cudaError_t check_cuda ( cudaError_t, uint ) ;

// Events and Streams
#define cudaEventCreate(args...) check_cuda( cudaEventCreate( args ), __LINE__ )
#define cudaEventDestroy(args...) check_cuda( cudaEventDestroy( args ), __LINE__ )
#define cudaEventElapsedTime(args...) check_cuda( cudaEventElapsedTime( args ), __LINE__ )
#define cudaEventRecord(args...) check_cuda( cudaEventRecord( args ), __LINE__ )
#define cudaEventSynchronize(args...) check_cuda( cudaEventSynchronize( args ), __LINE__ )

#define cudaStreamCreate(args...) check_cuda( cudaStreamCreate( args ), __LINE__ )
#define cudaStreamDestroy(args...) check_cuda( cudaStreamDestroy( args ), __LINE__ )

// Memory allocations
#define cudaFree(args...) check_cuda( cudaFree( args ), __LINE__ )
#define cudaMalloc(args...) check_cuda( cudaMalloc( args ), __LINE__ )
#define cudaMallocHost(args...) check_cuda( cudaMallocHost( args ), __LINE__ )
#define cudaMemcpy(args...) check_cuda( cudaMemcpy( args ), __LINE__ )
#define cudaMemset(args...) check_cuda( cudaMemset( args ), __LINE__ )

// Properties
#define cudaGetDeviceProperties(args...) check_cuda( cudaGetDeviceProperties( args ), __LINE__ )
#define cudaSetDevice(args...) check_cuda( cudaSetDevice( args ), __LINE__ )

// DEBUG PRINT
#define DEBUG_PRINT(fmt, args...) \
	printf("%sDEBUG:%s %s:%d:%s(): " fmt, _red, _reset, __FILE__, __LINE__, __func__, ##args)

#define _breakpoint() \
	{ fflush( stdout ); printf("Press any key to continue\n") ; getchar(); }

#else

#define _breakpoint()
#define DEBUG_PRINT(fmt, args...) // Don't do anything

#endif

// }}}

// {{{ MEM_DEBUG --
#ifdef MEM_DEBUG
void *debug_mem_malloc ( uint, const char *, uint ) ;
void debug_mem_free ( void *, const char *, uint ) ;

#define malloc(n) debug_mem_malloc( n, __func__, __LINE__ )
#define free(n) debug_mem_free( n, __func__, __LINE__ ) 
#endif
// }}}
