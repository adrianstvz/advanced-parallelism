/*
 * Implementation of common functions used on the HeterogeneusProgramming assignments
 *
 * By: 		Adrian Estevez Barreiro
 * Mail:	adrian@adrianstvz.es
 * Year: 	2020
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "cu_debug.h"


// {{{ TIME
void 
myElapsedtime ( resnfo start, resnfo end, timenfo *t ) {
#ifdef _noWALL_
	t->time = (end.ru_utime.tv_sec + (end.ru_utime.tv_usec * 1E-6)) 
		- (start.ru_utime.tv_sec + (start.ru_utime.tv_usec * 1E-6));
	t->systime = (end.ru_stime.tv_sec + (end.ru_stime.tv_usec * 1E-6)) 
		- (start.ru_stime.tv_sec + (start.ru_stime.tv_usec * 1E-6));
#else
	*t = (end.tv_sec + (end.tv_usec * 1E-6)) 
		- (start.tv_sec + (start.tv_usec * 1E-6));
#endif /*_noWALL_*/
}
// }}}


// {{{ CHECK CUDA --
cudaError_t
check_cuda (cudaError_t result, uint line) {
	if (result != cudaSuccess) {
		fprintf(stderr, "line: %d - CUDA Runtime Error: %s\n", line, cudaGetErrorString(result));
		assert(result == cudaSuccess);
	}
	return result;
}
// }}}


// {{{ FUNCTIONS

// {{{ device_info --
void
device_info ( void ) {
	/* Prints Device Properties */

	cudaDeviceProp prop;

	cudaGetDeviceProperties(&prop, 0);

	printf("-> CUDA Pkatform & Capabilities\n");
	printf("Name: %s\n", prop.name);
	printf("totalGlobalMem: %.2f MB\n", prop.totalGlobalMem/1024.0f/1024.0f);
	printf("sharedMemPerBlock: %.2f KB\n", prop.sharedMemPerBlock/1024.0f);
	printf("regsPerBlock (32 bits): %d\n", prop.regsPerBlock);
	printf("warpSize: %d\n", prop.warpSize);
	printf("memPitch: %.2f KB\n", prop.memPitch);
	printf("maxThreadsPerBlock: %d\n", prop.maxThreadsPerBlock);
	printf("maxThreadsDim: %d x %d x %d\n",
			prop.maxThreadsDim[0], prop.maxThreadsDim[1], prop.maxThreadsDim[2] );
	printf("maxGridSize: %d x %d\n", prop.maxGridSize[0], prop.maxGridSize[1]);
	printf("totalConstMem: %.2f KB\n", prop.totalConstMem);
	printf("major.minor: %d.%d\n", prop.major, prop.minor);
	printf("clockRate: %.2f MHz\n", prop.clockRate / 1024.0f);
	printf("textureAlignment: %d\n", prop.textureAlignment);
	printf("deviceOverlap: %d\n", prop.deviceOverlap);
	printf("multiProcessorCount: %d\n", prop.multiProcessorCount);

}
// }}}

// {{{ check_params --
void
check_params ( uint *n, uint *blocks ) {
	/* Function to adjust the number of threads and blocks according to the GPU properties */

	cudaDeviceProp prop;

	if ( *blocks > *n ) *blocks = *n;

	cudaGetDeviceProperties(&prop, 0);

	if ( *blocks > prop.maxThreadsDim[0] ) {
		*blocks = prop.maxThreadsDim[0];
		printf(" ->Number of threads/block modified to %d (max per block for dev)\n", *blocks);
	}	

	if ((( *n + *blocks -1 ) / *blocks ) > prop.maxGridSize[0] ) {
		*blocks = 2 * (*n - 1 ) / ( prop.maxGridSize[0] - 1 );
		if ( *blocks > prop.maxThreadsDim[0] ) {
			*blocks = prop.maxThreadsDim[0];
			printf(" ->Number of threads/block modified to %d (max per block for dev)\n", *blocks);

			if ( *n > (prop.maxGridSize[0] * *blocks )) {
				*n = prop.maxGridSize[0] * *blocks;
				printf(" ->Total number of threads modified to %d (max per grid for dev)\n", *n);
			} else
				printf("\n");
		} else 
			printf(" ->Number of threads/block modified to %d (%d max block/grid for dev)\n\n",
					*blocks, prop.maxGridSize[0]);
	}
}
// }}}

// }}}


// {{{ MEM TESTS
// Undef free and malloc to avoid infinite loop
#undef malloc
#undef free

// MALLOC
void *
debug_mem_malloc ( uint size, const char *func, uint line ) {

	void *ptr;

	if ( (ptr = malloc(size)) == NULL ) {
		printf("MEM ERROR: Malloc returns null trying to allocate %u bytes at line %u (func: %s())\n",
				size, line, func );
		exit(0);
	}

	printf(" -> Allocated %u bytes on line %u (func: %s())\n",
			size, line, func );

	return ptr;

}

// FREE
void 
debug_mem_free ( void *ptr, const char *func, uint line ) {

	printf(" -> Freeing memory on line %u (func: %s())\n",
			line, func );
	free(ptr);

}

// }}}

