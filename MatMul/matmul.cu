/*
 * Matrix multiplication
 * 
 * By: 		Adrian Estevez Barreiro
 * Mail:	adrian@adrianstvz.es
 * Year: 	2021
 * */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

// Debug Library
#include "cu_debug.h"

typedef struct {
	uint width;
	uint height;
	size_t mem_size;
	basetype *elements;
} Matrix;

typedef struct {
	int gpu_id;
	int offset[2];
	Matrix A;
	Matrix B;
	Matrix C;
	cudaStream_t stream;
} GPUWork;

// -- Declare Global Variables
#define TILE_DIM 16
//#define INIT_VALUE ( rand() % 5 )
#define INIT_VALUE 5.0f


uint Arows;
uint Acols;
uint Bcols;
const uint Msize = 1 << 10;

__constant__ int device_id[1];

// -- Declare Functions
// Device
__global__ void matmul_kernel ( Matrix, const Matrix, const Matrix ) ;

// Host
void args_parse ( int, char ** ) ;
void check_args ( void ) ;
int compare_matrix ( const Matrix, const Matrix ) ;
void cpu_matmul ( Matrix, const Matrix, const Matrix ) ;
void gpu_matmul_seq ( Matrix, const Matrix, const Matrix ) ;
void gpu_matmul_mult ( Matrix, const Matrix, const Matrix, int ) ;
void init_matrix ( Matrix ) ;
void print_matrix ( Matrix ) ;


// {{{ MAIN CODE --
int
main ( int argc, char **argv ) {

	DEBUG_PRINT("Debugging is enabled.\n");

	int ngpus;
	cudaGetDeviceCount( &ngpus );

	printf( "Number of GPUs used: %d\n", ngpus );

	// Time vars
	resnfo 	start, end; // sGPU, eGPU;
	timenfo time; // tGPU;

	// Read input
	args_parse( argc, argv );


//	if ( !size ) {
//		device_info();
//		exit( EXIT_SUCCESS );
//	}

	//check_args();
	if ( ( Acols % ( TILE_DIM ) ) || 
			( Arows % ( TILE_DIM * ngpus )) || 
			( Bcols % ( TILE_DIM )) ) {
		printf( "Rows and Columns must be multiples of %d\n", TILE_DIM * ngpus );
		exit( EXIT_FAILURE );
	}

	// Create Matrix
	Matrix h_A, h_B, h_C, h_O;
	h_A.height = Arows; h_A.width = Acols; 
	h_A.mem_size = sizeof( h_A.elements ) * h_A.width * h_A.height;

	h_B.height = Acols; h_B.width = Bcols;
	h_B.mem_size = sizeof( h_B.elements ) * h_B.width * h_B.height;

	h_C.height = Arows; h_C.width = Bcols;
	h_C.mem_size = sizeof( h_C.elements ) * h_C.width * h_C.height;

	h_O.height = Arows; h_O.width = Bcols;
	h_O.mem_size = sizeof( h_O.elements ) * h_O.width * h_O.height;

	printf( "C ( %d x %d ) = A ( %d x %d ) x B ( %d x %d ) \n",
			h_C.height, h_C.width, h_A.height, h_A.width, h_B.height, h_B.width );

	// Allocate memory
	h_A.elements = ( basetype * ) malloc( h_A.mem_size );
	h_B.elements = ( basetype * ) malloc( h_B.mem_size );
	h_C.elements = ( basetype * ) malloc( h_C.mem_size );
	h_O.elements = ( basetype * ) malloc( h_O.mem_size );

	init_matrix( h_A );
	init_matrix( h_B );

	// -- multiply sequential
	timestamp(&start);
	cpu_matmul( h_O, h_A, h_B );
	//gpu_matmul_seq( h_O, h_A, h_B );

	timestamp(&end);
	myElapsedtime(start, end, &time);
	printtime(time);
	printf(" -> Sequential Multiplication\n\n");
	// Done sequential

	// -- multiply GPU
	timestamp(&start);

	gpu_matmul_mult( h_C, h_A, h_B, ngpus );
	//gpu_matmul_seq( h_C, h_A, h_B );

	timestamp(&end);
	myElapsedtime(start, end, &time);
	printtime(time);
	printf(" -> GPU Multiplication\n\n");

	//print_matrix( h_O );
	//print_matrix( h_C );

	compare_matrix( h_C, h_O );


	free( h_A.elements );
	free( h_B.elements );
	free( h_C.elements );

	return 0;

}

// }}} 

// {{{ KERNELS --

// {{{ matmul_kernel --
__global__ void 
matmul_kernel ( Matrix C, const Matrix A, const Matrix B ) {
// Kernel to multiply 2 matrixes

	uint x = blockIdx.x * blockDim.x + threadIdx.x ;
	uint y = blockIdx.y * blockDim.y + threadIdx.y ;

	basetype value = 0;

	if ( y >= C.height || x >= C.width ) return;

	for ( uint i = 0; i < A.width; i++ ) {

		value += A.elements[ y*A.width + i ] * B.elements[ x + i*B.width ];

	}

	/*
	DEBUG_PRINT( "Device %d | block ( %d, %d ) -> writes %lf on position ( %d, %d )\n", 
					*device_id, blockIdx.x, blockIdx.y, value, x, y );
	*/

	C.elements[ y*C.width + x ] = value;

}
// }}} 

// }}} 

// {{{ FUNCTIONS --

// {{{ args_parse --
void
args_parse ( int argc, char **argv ) {
// Parse the arguments send to the program

	Arows = Acols = Bcols = Msize;

	Acols = ( argc > 1 ) ? atoi( argv[1] ) : Msize;
	Arows = ( argc > 2 ) ? atoi( argv[2] ) : Msize;
	Bcols = ( argc > 3 ) ? atoi( argv[3] ) : Msize;

	/*
	while ( --argc > 0 ) {
		if ( (*++argv)[0] == '-' ) {
			switch ( opt ) {
				default:
					break;
			}
		}
	}
	*/
}
// }}}

// {{{ check_args --
void 
check_args ( void ) {

	if ( ( Acols % TILE_DIM ) || ( Arows % TILE_DIM ) || ( Bcols % TILE_DIM ) ) {
		printf( "Rows and Columns must be multiples of %d\n", TILE_DIM );
		exit( EXIT_FAILURE );
	}

}
// }}}

// {{{ compare_matrix --
int
compare_matrix ( const Matrix A, const Matrix B ) {
// Compare two matrixes

	if ( A.height != B.height && A.width != B.width ) {
		printf( "Error!! Matrix A and B have different sizes!\n");
		return 1;
	}

	uint errors = 0;
	for ( uint i = 0; i < A.height * A.width; i++ ) {
		errors += ( A.elements[i] != B.elements[i] );
	}

	if ( errors ) {
		printf( "Error!! %d error%sfound\n", errors, ( errors > 1 ) ? "s " : " " );
		return 1;
	} else {
		printf( "Success!! No errors found\n" );
	}
	return 0;

}
// }}}

// {{{ cpu_matmul --
void
cpu_matmul ( Matrix C, const Matrix A, const Matrix B ) {
// CPU Matrix multiplication

	basetype value = 0;

	for ( uint i = 0; i < A.height; i++ )
		for ( uint j = 0; j < B.width; j++ ) {
			value = 0;
			for ( uint k = 0; k < A.width; k++ )
				value += A.elements[ i*A.width + k ] * B.elements[ k*B.width + j ];
			C.elements[ i * C.width + j ] = value;
		}

}
// }}}

// {{{ gpu_matmul_seq --
void
gpu_matmul_seq ( Matrix h_C, const Matrix h_A, const Matrix h_B ) {
// GPU Matrix multiplication

	// Prepare GPU Data
	Matrix d_A, d_B, d_C;

	d_A.height = h_A.height;
	d_A.width = h_A.width;
	d_A.mem_size = h_A.mem_size;

	d_B.height = h_B.height;
	d_B.width = h_B.width;
	d_B.mem_size = h_B.mem_size;

	d_C.height = h_C.height;
	d_C.width = h_C.width;
	d_C.mem_size = h_C.mem_size;

	// Allocate GPU memory
	cudaMalloc( &d_A.elements, d_A.mem_size );
	cudaMalloc( &d_B.elements, d_B.mem_size );
	cudaMalloc( &d_C.elements, d_C.mem_size );
	
	// Send data to GPU
	cudaMemcpy( d_A.elements, h_A.elements, d_A.mem_size, cudaMemcpyHostToDevice );
	cudaMemcpy( d_B.elements, h_B.elements, d_B.mem_size, cudaMemcpyHostToDevice );

	// Execute kernel
	dim3 dimBlock( TILE_DIM, TILE_DIM );
	dim3 dimGrid( ( d_B.width + dimBlock.x - 1 )/dimBlock.x,
				  ( d_A.height + dimBlock.y - 1 )/dimBlock.y );

	matmul_kernel <<< dimGrid, dimBlock >>> ( d_C, d_A, d_B );
	
	cudaMemcpy( h_C.elements, d_C.elements, d_C.mem_size, cudaMemcpyDeviceToHost );

	// Free Memory
	cudaFree( d_A.elements );
	cudaFree( d_B.elements );
	cudaFree( d_C.elements );
		
}
// }}}

// {{{ gpu_matmul_mult --
void
gpu_matmul_mult ( Matrix h_C, const Matrix h_A, const Matrix h_B, int ngpus ) {
// GPU Matrix multiplication

	// Prepare GPU Data
	GPUWork	works[ngpus];

	// Init gpu works
	for ( uint i = 0; i < ngpus; i++ ) {

		works[i].gpu_id = i;

		cudaSetDevice( works[i].gpu_id );
		cudaMemcpyToSymbol ( device_id, &works[i].gpu_id, sizeof( i ) );

		cudaStreamCreate( &works[i].stream );

		Matrix d_A, d_B, d_C;

		d_A.width = h_A.width;
		d_A.height = ( h_A.height / ngpus );
		d_A.mem_size = sizeof( d_A.elements ) * d_A.width * d_A.height;

		d_B.height = h_B.height;
		d_B.width = h_B.width;
		d_B.mem_size = h_B.mem_size;

		d_C.width = h_C.width;
		d_C.height = ( h_C.height / ngpus );
		d_C.mem_size = sizeof( d_C.elements ) * d_C.width * d_C.height;

		// Allocate GPU memory
		cudaMalloc( &d_A.elements, d_A.mem_size );
		cudaMalloc( &d_B.elements, d_B.mem_size );
		cudaMalloc( &d_C.elements, d_C.mem_size );

		works[i].A = d_A;
		works[i].B = d_B;
		works[i].C = d_C;

		works[i].offset[0] = i * ( d_A.width * d_A.height );
		works[i].offset[1] = i * ( d_C.width * d_C.height );

	}

	// Execute works on each CPU
	for ( uint i = 0; i < ngpus; i++ ) {

		cudaSetDevice( works[i].gpu_id );

		// Execute kernel
		dim3 dimBlock( TILE_DIM, TILE_DIM );
		dim3 dimGrid( ( works[i].B.width + dimBlock.x - 1 )/dimBlock.x,
					  ( works[i].A.height + dimBlock.y - 1 )/dimBlock.y );

		DEBUG_PRINT("Size of Grid %d is %dx%d\n", i, dimGrid.y, dimGrid.x);
	
		// Send data to GPU
		cudaMemcpyAsync( works[i].B.elements, h_B.elements, 
							works[i].B.mem_size, cudaMemcpyHostToDevice, works[i].stream );

		cudaMemcpyAsync( works[i].A.elements, h_A.elements + works[i].offset[0],
							works[i].A.mem_size, cudaMemcpyHostToDevice, works[i].stream );
	
		printf( " Launching device %d\n", works[i].gpu_id );
		matmul_kernel <<< dimGrid, dimBlock, 0, works[i].stream >>> ( works[i].C, works[i].A, works[i].B );

		cudaMemcpyAsync( h_C.elements + works[i].offset[1], works[i].C.elements,
							works[i].C.mem_size, cudaMemcpyDeviceToHost, works[i].stream );

	}

	// Destroy Stream and Free Memory
	for ( uint i = 0; i < ngpus; i++ ) {

		cudaSetDevice( works[i].gpu_id );
      	cudaStreamSynchronize( works[i].stream );
		cudaStreamDestroy( works[i].stream );

		// Free Memory
		cudaFree( works[i].A.elements );
		cudaFree( works[i].B.elements );
		cudaFree( works[i].C.elements );

	}

		
}
// }}}

// {{{ init_matrix --
void
init_matrix ( Matrix M ) {
// Initialize a matrix elements

	for ( int i = 0; i < M.height; i++ )
		for ( int j = 0; j < M.width; j++ )
			M.elements[ i * M.width + j ] = INIT_VALUE;

}
// }}} 

// {{{ print_matrix --
void
print_matrix ( Matrix M ) {
// Print a Matrix

	printf( "Printing a %d by %d matrix: \n", M.height, M.width );
	for ( uint i = 0; i < M.height * M.width; i++ )
		printf( "%4.4f%c", M.elements[i], ( (i+1) % M.width ) ? ' ' : '\n' );

}
// }}}

// }}} 

