#!/bin/sh
#----------------------------------------------------
#SBATCH -J gpu_job
#SBATCH -o %x-%j.log
#SBATCH -e %x-%j.err
#SBATCH -c 1
#SBATCH -p gpu-shared
#SBATCH --gres=gpu:2
#SBATCH -t 00:30:00


DIR=.
NAME=malmut
LIBS=${DIR}/cu_debug

# Read Args
NAME=${1:-NAME}

# LOAD MODULE
module load cuda/10.0.130

# COMPILE
nvcc -o ${DIR}/${NAME}.{out,cu} ${LIBS}.cu

./${DIR}/${NAME}.out 
